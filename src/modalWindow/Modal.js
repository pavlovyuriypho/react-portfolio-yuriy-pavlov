import React from "react";
import './Modal.css';
export default class Modal extends React.Component {
    state = {
        isOpen: false,
    }
    render() {
        return(
            <React.Fragment>
                <button className="btnOpenModal" onClick = {() => this.setState({ isOpen: true })}>
                    Open modal window
                </button>

                {this.state.isOpen && (<div className = 'modal'>
                    <div className = 'modal-window'>
                        <h2>Modal</h2>
                        <p>This is modal window</p>
                        <button className="closeModal" onClick = {() => this.setState({ isOpen: false })}>
                            Close modal window
                        </button>
                    </div>
                </div>)}
            </React.Fragment>
        )
    }
}